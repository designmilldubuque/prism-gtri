# README #

### What is this repository for? ###

This repository is for the GTRI PRISM demonstration application that will be shown at I/ITSEC 2015.  Users of the app can point the app to the PRC 152 image marker and then visualize a ditigal PRC 152 headset being wired up.  Currently the app has only been built out in iOS.  The provisioning profiles are already setup, the only setup task you will have to do is import the developer credentials in xCode before building out.  I distriuted the application through Diawi.

Diawi.com

### How do I get set up? ###

* Summary of set up
- Clone this repository into your local SourceTree repository
- Open in Unity 5.2.x
- Change the settings of QCARWrapper.bundle in the Unity inspector from "Any Platform" to "Standalone + Editor" (i.e. tick both the Standalone and the Editor checkboxes).
- Build out for xCode 7.x (download xCode 7.x if you don't have this installed)
- Import the current certificates and provisioning profiles (xCode -> Preferences).  Do not create new certs or provisioning profiles.
- Link the MessageUI.framework

### Who do I talk to? ###

Repo owner: Sam Murley
GTRI: Erik Strachan