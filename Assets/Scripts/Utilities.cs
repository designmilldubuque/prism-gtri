﻿using UnityEngine;
using System.Collections;
using System;
using System.IO;
using IndieYP;
using System.Collections.Generic;

public class Utilities : MonoBehaviour {

	private string strStreamingFolder;

	// Use this for initialization
	void Start () {
		strStreamingFolder = PDFReader.AppDataPath + "/";
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void OpenURL(string strURL)
	{
		Application.OpenURL(strURL);
	}

	public void PlayMovie(string strName)
	{
		Handheld.PlayFullScreenMovie (strName, Color.black, FullScreenMovieControlMode.CancelOnInput);
	}

	public void OpenPDF(string strName)
	{
		PDFReader.OpenDocInWebViewLocal(strStreamingFolder + strName, "Harris AN/PRC-152", "Back", true);
	}
}
