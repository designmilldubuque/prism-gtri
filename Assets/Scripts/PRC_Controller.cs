﻿using UnityEngine;
using System.Collections;

public class PRC_Controller : MonoBehaviour {

	public GameObject goPRC;


	// Use this for initialization
	void Start () 
	{

	}
	
	// Update is called once per frame
	void Update () 
	{
		
	}

	//Play PRC Animation
	public void PlayAnimation (string strAnimation)
	{
		goPRC.GetComponent<Animator>().Play(strAnimation);
	}
}
