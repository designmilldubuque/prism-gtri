﻿using UnityEngine;
using System.Collections;
using System;
using System.IO;
using IndieYP;

public class Utilities1 : MonoBehaviour {

	private string strStreamingFolder;
	private bool isInteractingWithModel = false;
	private DevicePart device;
	private GameObject hand;
	private GameObject imageTarget;
	private Vector3 rotationDirection;

	// Use this for initialization
	void Start () {
		strStreamingFolder = PDFReader.AppDataPath + "/";
		device = new DevicePart("PRC_152 Animation");
		if (device != null && device.DeviceGO != null) {
			Debug.Log("device position = " + device.DeviceGO.transform.position.x + "," + device.DeviceGO.transform.position.y + "," + device.DeviceGO.transform.position.z);
			Debug.Log("device scale = " + device.DeviceGO.transform.localScale.x + "," + device.DeviceGO.transform.localScale.y + "," + device.DeviceGO.transform.localScale.z);
			Debug.Log("device restore position = " + device.RestoreToPosition.x + "," + device.RestoreToPosition.y + "," + device.RestoreToPosition.z);
			Debug.Log("device restore scale = " + device.RestoreToScale.x + "," + device.RestoreToScale.y + "," + device.RestoreToScale.z);
		}
		hand = GameObject.Find("Hand");
		imageTarget = GameObject.Find ("ImageTarget");
		rotationDirection = new Vector3(0, 1, 1);
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void OpenURL(string strURL)
	{
		Application.OpenURL(strURL);
	}

	public void PlayMovie(string strName)
	{
		Handheld.PlayFullScreenMovie (strName, Color.black, FullScreenMovieControlMode.CancelOnInput);
	}

	public void OpenPDF(string strName)
	{
		PDFReader.OpenDocInWebViewLocal(strStreamingFolder + strName, "Harris AN/PRC-152", "Back", true);
	}

	public void ToggleModelInteraction() {

		isInteractingWithModel = !isInteractingWithModel;

		if (device != null && device.DeviceGO != null) {
			if (isInteractingWithModel) {
				if (hand == null)
					hand = GameObject.Find("Hand");

				if (hand != null) {
					Renderer hr = hand.GetComponent<Renderer>();
					if (hr != null) {
						Debug.Log("hiding hand");
						hr.enabled = false;
					}
				}

				var pinchRecognizer = new TKPinchRecognizer();
				pinchRecognizer.gestureRecognizedEvent += (r) => {
					Debug.Log("pinch recognizer fired");
					device.DeviceGO.transform.localScale += device.RestoreToScale * pinchRecognizer.deltaScale;
				};
				
				TouchKit.addGestureRecognizer(pinchRecognizer);

				var oneFingerRotationRecognizer = new TKOneFingerRotationRecognizer();
				//oneFingerRotationRecognizer.targetPosition = Camera.main.WorldToScreenPoint(device.DeviceGO.transform.position);
				oneFingerRotationRecognizer.targetPosition = Camera.main.WorldToScreenPoint(device.RestoreToPosition);
				oneFingerRotationRecognizer.gestureRecognizedEvent += (r) => {
					Debug.Log("rotation recognizer fired");
					device.DeviceGO.transform.Rotate (rotationDirection, oneFingerRotationRecognizer.deltaRotation);
				};

				TouchKit.addGestureRecognizer(oneFingerRotationRecognizer);
			}	//interacting with model
			else {
				Debug.Log("remove touchkit recognizers");
				TouchKit.removeAllGestureRecognizers();
			}
		}

	}

	public void ResetModel() {
		if (device != null && device.DeviceGO != null) {
			Debug.Log("reset model");
			device.DeviceGO.transform.position = device.RestoreToPosition;
			device.DeviceGO.transform.localScale = device.RestoreToScale;
		}

		if (hand != null) {
			Renderer hr = hand.GetComponent<Renderer>();
			if (hr != null) {
				Debug.Log("showing hand");
				hr.enabled = true;
			}
		}

	}


	public class DevicePart {
		public GameObject DeviceGO = null;
		public Vector3 RestoreToPosition;
		public Vector3 RestoreToScale;

		public DevicePart (string deviceName) {
			GameObject obj = GameObject.Find (deviceName);
			if (obj != null) {
				DeviceGO = obj;
				RestoreToPosition = obj.transform.position;
				RestoreToScale = obj.transform.localScale;
			}
		}
	}
}
